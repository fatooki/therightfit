<!DOCTYPE html>
<?php
session_start();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
        <script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >     
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"> 
         <link href="css/user_form.css" rel="stylesheet" type="text/css"/>     
        <link href="css/mygrid.css" rel="stylesheet" type="text/css"/>
        <link href="css/work.css" rel="stylesheet" type="text/css"/>
        <link href="css/interview.css" rel="stylesheet" type="text/css"/>
        <link href="css/form_button.css" rel="stylesheet" type="text/css"/>
        <link href="css/profileCard.css" rel="stylesheet" type="text/css"/>
        <link href="css/question.css" rel="stylesheet" type="text/css"/>
             <title>The Right Fit</title>
    </head>
        <body>
          
        
       
    
            <div id="content">
                <div id="banner" class="overlaycolor">
                    <header>                                  
                        <a class="clear-padding" href="#">
                            <div class="logo" >
                                <img src="image/puzzle.png" height="50" width="50" alt="logo"/>
                            </div>   
                        </a>
                        <h1 class="title">The Right Fit</h1>       
                        <nav>
                            <div class="nav-right">                   
                                <a  href="home.php?page=9">Contact</a>
                                <a  href="#">About</a>
                                <a href="home.php?page=10">How To</a>
                                    <a href="home.php?page=0" ><i class="fas fa-sign-out-alt" style="font-size:20px"></i>Log out</a>    
                            </div>
                        </nav>
                    </header>
                </div>

                <sidenav>
                    <?php
                    if (isset($_GET['page']) && isset($_SESSION['user_id'])) {
                        switch ($_GET['page']) {
                               case 0:
                                ?>
                                <div class="list-group list-group-mine">
                                    <a href="home.php?page=7" class="list-group-item"><i class="fa fa-user-circle fa-lg"></i> <span>Register</span></a>
                                    <a href="home.php?page=8" class="list-group-item"><i class="fa fa-credit-card"></i> <span>About</span></a>
                                </div>
                            <?php
                                break;
                            case 1:
                                ?>
                                <div class="list-group list-group-mine">
                                    <a href="home.php?page=1" class="list-group-item active"><i class="fa fa-user-circle fa-lg"></i> <span>Profile</span></a>
                                    <a href="home.php?page=5" class="list-group-item"><i class="fa fa-map fa-lg"></i> <span>Location</span></a>
                                    <a href="home.php?page=2" class="list-group-item"><i class="fa fa-credit-card"></i> <span>Work History</span></a>
                                    <a href="home.php?page=3" class="list-group-item"><i class="fa fa-question-circle"></i> <span>Education</span></a>
                                    <a href="home.php?page=4&q_page=1" class="list-group-item"><i class="fas fa-video"></i> <span>Interview</span></a>                                
                                    <a href="home.php?page=6" class="list-group-item"><i class="fa fa-compass"></i> <span>Review</span></a>
                                </div>
                                <?php
                                break;
                            case 2:
                                ?>
                                <div class="list-group list-group-mine">
                                    <a href="home.php?page=1" class="list-group-item "><i class="fa fa-user-circle fa-lg"></i> <span>Profile</span></a>
                                    <a href="home.php?page=5" class="list-group-item "><i class="fa fa-map fa-lg"></i> <span>Location</span></a>
                                    <a href="home.php?page=2" class="list-group-item active"><i class="fa fa-credit-card"></i> <span>Work History</span></a>
                                    <a href="home.php?page=3" class="list-group-item "><i class="fa fa-question-circle"></i> <span>Education</span></a>
                                    <a href="home.php?page=4&q_page=1" class="list-group-item "><i class="fas fa-video"></i> <span>Interview</span></a>                              
                                    <a href="home.php?page=6" class="list-group-item"><i class="fa fa-compass"></i> <span>Review</span></a>
                                </div>
                                <?php
                                break;
                            case 3:
                                ?>
                                <div class="list-group list-group-mine">
                                    <a href="home.php?page=1" class="list-group-item "><i class="fa fa-user-circle fa-lg"></i> <span>Profile</span></a>
                                    <a href="home.php?page=5" class="list-group-item"><i class="fa fa-map fa-lg"></i> <span>Location</span></a>
                                    <a href="home.php?page=2" class="list-group-item"><i class="fa fa-credit-card"></i> <span>Work History</span></a>
                                    <a href="home.php?page=3" class="list-group-item active"><i class="fa fa-question-circle"></i> <span>Education</span></a>
                                    <a href="home.php?page=4&q_page=1" class="list-group-item"><i class="fas fa-video"></i> <span>Interview</span></a>

                                    <a href="home.php?page=6" class="list-group-item"><i class="fa fa-compass"></i> <span>Review</span></a>
                                </div>
                                <?php
                                break;
                            case 4:
                                ?>
                                <div class="list-group list-group-mine">
                                    <a href="home.php?page=1" class="list-group-item "><i class="fa fa-user-circle fa-lg"></i> <span>Profile</span></a>
                                    <a href="home.php?page=5" class="list-group-item"><i class="fa fa-map fa-lg"></i> <span>Location</span></a>
                                    <a href="home.php?page=2" class="list-group-item"><i class="fa fa-credit-card"></i> <span>Work History</span></a>
                                    <a href="home.php?page=3" class="list-group-item"><i class="fa fa-question-circle"></i> <span>Education</span></a>
                                    <a href="home.php?page=4&q_page=1" class="list-group-item active"><i class="fas fa-video"></i> <span>Interview</span></a>                              
                                    <a href="home.php?page=6" class="list-group-item"><i class="fa fa-compass"></i> <span>Review</span></a>
                                </div>
                                <?php
                                break;
                            case 5:
                                ?>
                                <div class="list-group list-group-mine">
                                    <a href="home.php?page=1" class="list-group-item "><i class="fa fa-user-circle fa-lg"></i> <span>Profile</span></a>
                                    <a href="home.php?page=5" class="list-group-item active"><i class="fa fa-map fa-lg"></i> <span>Location</span></a>
                                    <a href="home.php?page=2" class="list-group-item"><i class="fa fa-credit-card"></i> <span>Work History</span></a>
                                    <a href="home.php?page=3" class="list-group-item"><i class="fa fa-question-circle"></i> <span>Education</span></a>
                                    <a href="home.php?page=4&q_page=1" class="list-group-item"><i class="fas fa-video"></i> <span>Interview</span></a>                               
                                    <a href="home.php?page=6" class="list-group-item"><i class="fa fa-compass"></i> <span>Review</span></a>
                                </div>
                                <?php
                                break;
                            case 6:
                                ?>
                                <div class="list-group list-group-mine">
                                    <a href="home.php?page=1" class="list-group-item "><i class="fa fa-user-circle fa-lg"></i> <span>Profile</span></a>
                                    <a href="home.php?page=5" class="list-group-item"><i class="fa fa-map fa-lg"></i> <span>Location</span></a>
                                    <a href="home.php?page=2" class="list-group-item"><i class="fa fa-credit-card"></i> <span>Work History</span></a>
                                    <a href="home.php?page=3" class="list-group-item"><i class="fa fa-question-circle"></i> <span>Education</span></a>
                                    <a href="home.php?page=4&q_page=1" class="list-group-item"><i class="fas fa-video"></i> <span>Interview</span></a>                             
                                    <a href="home.php?page=6" class="list-group-item active"><i class="fa fa-compass"></i> <span>Review</span></a>
                                </div>
                                <?php
                                break;
                            case 7:
                                ?>
                                <div class="list-group list-group-mine">
                                    <a href="home.php?page=7" class="list-group-item active "><i class="fa fa-user-circle fa-lg"></i> <span>Register</span></a>
                                    <a href="home.php?page=8" class="list-group-item"><i class="fa fa-credit-card"></i> <span>About</span></a>
                                </div>
                            <?php
                                break;
                            case 8:
                                ?>
                                <div class="list-group list-group-mine">
                                    <a href="home.php?page=7" class="list-group-item "><i class="fa fa-user-circle fa-lg"></i> <span>Register</span></a>
                                    <a href="home.php?page=8" class="list-group-item active"><i class="fa fa-credit-card"></i> <span>About</span></a>
                                </div>

                            <?php
                                break;
                            case 9:
                                ?>
                                <div class="list-group list-group-mine">
                                    <a href="home.php?page=1" class="list-group-item active "><i class="fa fa-user-circle fa-lg"></i> <span>Home</span></a>
<!--                                    <a href="home.php?page=8" class="list-group-item"><i class="fa fa-credit-card"></i> <span>About</span></a>-->
                                </div>
                            <?php
                             break;
                            case 11:
                                ?>
                                <div class="list-group list-group-mine">
                                  <a href="home.php?page=1" class="list-group-item "><i class="fa fa-user-circle fa-lg"></i> <span>Profile</span></a>
                                    <a href="home.php?page=5" class="list-group-item"><i class="fa fa-map fa-lg"></i> <span>Location</span></a>
                                    <a href="home.php?page=2" class="list-group-item"><i class="fa fa-credit-card"></i> <span>Work History</span></a>
                                    <a href="home.php?page=3" class="list-group-item"><i class="fa fa-question-circle"></i> <span>Education</span></a>
                                    <a href="home.php?page=4&q_page=1" class="list-group-item"><i class="fas fa-video"></i> <span>Interview</span></a>                             
                                    <a href="home.php?page=6" class="list-group-item"><i class="fa fa-compass"></i> <span>Review</span></a>
                                </div>
                            <?php
                            break;
                            default:
                            break;
                        }
                    } else {
                        ?>
                        <div class="list-group list-group-mine">
                            <a href="home.php?page=7" class="list-group-item "><i class="fa fa-user-circle fa-lg"></i> <span>Register</span></a>
                            <a href="home.php?page=8" class="list-group-item"><i class="fa fa-credit-card"></i> <span>About</span></a>
                        </div>
    <?php
}
?>  
             </sidenav>
                <?php

                if (isset($_GET['page']) && isset($_SESSION['user_id'])) {
                    switch ($_GET['page']) {
                        case 0:                        
                            print_r($_SESSION);
                            unset($_SESSION);
                            session_destroy();
                              header("Location:home.php");
                            break;
                        case 1:
                            include('inc_user_profile.php');
                            break;
                        case 2:
                            include('inc_work_history.php');
                            break;
                        case 3:
                            include('user_education.php');
                            break;
                        case 4:
                            include('inc_questions.php');
                            break;
                        case 5:
                            include('location.php');
                            break;
                        case 6:
                            include('play_vid.php');
                            break;
                        case 7;
                            include('reg_info.php');
                            break;
                        case 8:
                            include('about.php');
                            break;
                        case 9:
                            include('contact.php');
                            break;
                        case 10:
                            include('howto.php');
                            break;
                           case 11:
                            include('inc_upload_image.php');
                            break;
                        default:
                            break;
                    }
                } else {
                    ?>
                    <main>
                    <?php
                    include('login.php');
                    ?>
                    </main>
                        <?php
                    }
                    ?>
  

            </div>
<?php
// put your code here
?>

        </body>
        <footer>
            <div id="footer-banner" class="overlay2">                        
                &#169;
            </div>
        </footer>


    <script>
        var $input;

        function onInputFocus(event) {
            var $target = $(event.target);
            var $parent = $target.parent();
            $parent.addClass('input--filled');
        }
        ;

        function onInputBlur(event) {
            var $target = $(event.target);
            var $parent = $target.parent();

            if (event.target.value.trim() === '') {
                $parent.removeClass('input--filled');
            }
        }
        ;

        $(document).ready(function () {
            $input = $('.input__field');

            // in case there is any value already
            $input.each(function () {
                if ($input.val().trim() !== '') {
                    var $parent = $input.parent();
                    $parent.addClass('input--filled');
                }
            });

            $input.on('focus', onInputFocus);
            $input.on('blur', onInputBlur);
        });

        $(document).ready(function () {


            $('.list-group-item').click(function (e) {
                //            e.preventDefault();
                $('.list-group-item').removeClass('active');
                $(this).addClass('active');
            });
        });
    </script>
</html>
