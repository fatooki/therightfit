<?php
if (isset($_GET['msg'])) {
    switch ($_GET['msg']) {
        case 'regsuccess':
            echo '<script language="javascript">';
            echo 'alert(" Registration Complete")';
            echo '</script>';
            break;
        case 'loginsuccess':
            echo '<script language="javascript">';
            echo 'alert("USER UPDATED SUCCESSFULLY")';
            echo '</script>';
            break;
        case 'error':
            echo '<script language="javascript">';
            echo 'alert("The email you have entered is invalid, please try again.")';
            echo '</script>';
            break;
        case 'success':
            echo '<div class="statusmsg">' . $msg . '</div>'; // Display our message and wrap it with a div with the class "statusmsg".
            break;
        default:
    }
}
?>
<main>
    <page>
        <div class="page-middle">
            <div class="form-head-area">Registration</div>
            <form action="./reg_validation.php" method="POST" >
                <div id="reg-login-form">
                <div class="reg-login-text">
                    <label for="Firstname" >First Name:</label>   <br>                         
                    <input type="text" name="firstname" id="firstname" value=" <?php // echo $_POST['firstname']            ?>" required  name="firstname" />
                </div>
                <div class="reg-login-text">    
                    <label for="Lastname" >Last Name:</label>  <br>  
                    <input type="text"    name="lastname" id="lastname" value=" <?php // echo $_POST['lastname']            ?>" />
                </div>
                <div class="reg-login-text">    
                    <label for="email" >Email:</label>  <br>  
                    <input type="email"   name="email" id="email" required/>
                </div>
                <div class="reg-login-text">    
                    <label for="password" >Password:</label>  <br>  
                    <input type="password"    name="password" id="password" required />
                </div>
                <div class="reg-login-text">    
                    <label for="confirm_password" >Confirm Password:</label>  <br>  
                    <input type="password"   name="confirm_password" id="confirm_password" onkeyup="checkPass(); return false;">
                    <span id="confrimMessage" class="confirmMessage"></span>
                </div>
       
                    <input type="submit" id="registration" class="button" value="Register">
                
                    <button  id="login" type="button" class="button" onclick=" window.location.assign('home.php')">Log In</button>
     
                </div>
                 <div class="form-bottom-area">.</div>  
            </form>

        </div>
    </page>
    


</main>



<script>
    var password = document.getElementById("password")
            , confirm_password = document.getElementById("confirm_password");

    function validatePassword() {
        if (password.value !== confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>

</html>

