<!--this page loads the information completed from the registration process 
user must complete the rest of the information-->

<?php

include('mysqli_connect.php');
include('crud/image_process.php');

$select_user = "Select user.* , countries.name as country, regions.name as state, cities.name as city  
from user
left join countries on countries.id = country_id
left join regions on regions.id = state_id
left join cities on cities.id = city_id
 WHERE user_id=" . $_SESSION['user_id'];
    $result = $connection->query($select_user);
    $row = mysqli_fetch_assoc($result);
    if (isset($row['country'])) {
        $_SESSION['update'] = true;
    }
    $_SESSION['user_id'] = $row['user_id'];
    $_SESSION['contact_number'] = $row["phone"];
    $_SESSION['professional_title'] = $row['professional_title'];
    $_SESSION['how_many_years'] = $row['number_of_yrs'];
    $_SESSION['fname'] = $row['first_name'];
    $_SESSION['lname'] = $row['last_name'];
    $_SESSION['email_address'] = $row['email_address'];
    $_SESSION['uCountry'] = $row['country'];
    $_SESSION['uState'] = $row['state'];
    $_SESSION['uCity'] = $row['city'];
//    $connection->close();
    //  print_r($_SESSION);
    ?>
    <link href="css/user_form.css" rel="stylesheet" type="text/css"/>
    <main>  
       
            <page>
               
                <div class="page-left">
                     <form action="crud/process_user.php" method="POST">
                    <div class="form-head-area">
                        User Info:
                    </div>
                    <div id="form-container">                
                        <div class="left-text"><label for="first_name" >First Name</label><br><input type="text" name="first_name" value="<?php
                            If (isset($_SESSION['fname'])) {
                                echo $_SESSION['fname'];
                            }
                            ?>" /></div>
                        <div class ="middle-text"><label for="last_name">Last Name</label><br><input type="text" name="last_name" value="<?php
                            If (isset($_SESSION['lname'])) {
                                echo $_SESSION['lname'];
                            }
                            ?>" /></div>
                        <div class ="left-text"><label for="email" >Email Address</label><br><input type="text" name="email" value="<?php
                            If (isset($_SESSION['email_address'])) {
                                echo $_SESSION['email'];
                            }
                            ?>"/></div>
                        <div class="middle-text"><label for="contact_number">Contact Number</label><br><input type="text" name="contact_number" value="<?php
                            If (isset($_SESSION['contact_number'])) {
                                echo $_SESSION['contact_number'];
                            }
                            ?>" /></div>

                        <div class="left-text"><label for="professional_title">Professional Title</label><br><input type="text" name="professional_title" value="<?php
                            If (isset($_SESSION['professional_title'])) {
                                echo $_SESSION['professional_title'];
                            }
                            ?>"/>
                        </div>

                        <div class="middle-text">
                            <label for="how_many_years">Years In This Profession?<input type="number" name="how_many_years" min="0" max="30" value="<?php
                                If (isset($_SESSION['how_many_years'])) {
                                    echo $_SESSION['how_many_years'];
                                }
                                ?>"></label>
                        </div>
                        <button-area>
                            <input type="submit" class="button" value="Submit"/>
                        </button-area>              
                    </div>
                    <div class="form-bottom-area"></div> 
                      </form> 
                </div>
                
                 <?php
                 include'inc_profilecard.php';
                 create_user_path(); // get the location for picture and video
               ?>
                 
            </page>
      
    </main>
    </body>
    </html>
    <?php
//}

function phoneFormat($number) {
    if (ctype_digit($number) && strlen($number) == 10) {
        $number = substr($number, 0, 3) . '-' . substr($number, 3, 3) . '-' . substr($number, 6);
    } else {
        if (ctype_digit($number) && strlen($number) == 7) {
            $number = substr($number, 0, 3) . '-' . substr($number, 3, 4);
        }
    }
    return $number;
}
?>
<script type="text/javascript">
    var sessionTimeout = "<%= Session.Timeout %>";

    function DisplaySessionTimeout() {

        sessionTimeout = sessionTimeout - 1;

        if (sessionTimeout >= 0)
            window.setTimeout("DisplaySessionTimeout()", 60000);

        else
        {
            alert("Your current Session is over due to inactivity.");
        }
    }
</script>