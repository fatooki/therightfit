<link href="css/landing.css" rel="stylesheet" type="text/css"/>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link href="css/landing.css" rel="stylesheet" type="text/css"/>  
        <link href="css/button.css" rel="stylesheet" type="text/css"/>
        <script src="old_Scripts/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js" type="text/javascript"></script>

        <title>The Right Fit</title>
    </head>

    <body>
        <nav>
            <div class="menu">
                <img src="image/puzzle.png" alt="" height="50" width="50"/>
                <div class="label">The Right Fit</div>
                <div class="spacer"></div>
                <div class="item"><span>Contact</span></div>
                <div class="item"><span>Registration</span></div>
                <div class="item" onclick="window.location.href = 'home.php';"><span>Login</span></div>
            </div>
        </nav>    
        <main>
            <section class="cd-section cd-section--bg-fixed">              
                <div class="text-area">
                    <?php
                    if(isset($_SESSION['name'])){
                    $name = $_SESSION["name"];
                    }else{
                        $name = 'User';
                    }
                    echo "<h1> Congratulation! " .  $name . "</h1>";
                    echo '<p> Registration Successful... Please check email to confirm account </p>';
                    ?> 
                </div>
                <div id="button-grid">
                    <button class="btn-left" onclick="window.location.href = 'home.php';">Login</button>
                </div>
            </section>
            <section class="cd-section-border"> </section>

       </main>
    <footer>footer</footer>
</html>
