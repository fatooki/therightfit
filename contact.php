<?php ?>
<main>
    <page>
        <div class="page-middle">
            <div class="form-head-area">
                Send Message:
            </div>
            <div class="form-container">
                <div id="contact-form">
                    <form action="contact-send.php" method="post">
                    <div class="contact-info">
                       
                        <label for="name">Name:</label>
                        <input type="text" name="name" id="name">
                    </div>
                    <div class="contact-info">
                        <label for="from-email">Email:</label>
                        <input type="email" name="from-email" id="from-email">
                    </div>
                    <div class="contact-info">
                        <label for="type">Comment or Question?</label>
                        <select name="type">
                            <option value="">Select Option</option>
                            <option value="1">Comment</option>
                            <option value ="2">Question</option>
                        </select>
                    </div>
                    
                    <div class="contact-info">
                        <label for="message">Message</label>
                        <textarea name="message" rows="10" cols="50"></textarea>
                    </div>

            <button-area>
                <input type="submit" value="Send"/>
            </button-area>
       </form>    
                        </div>
            </div>   

            <div class="form-bottom-area"></div>
        </div>
           
    </page>
</main>