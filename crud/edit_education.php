<?php

if (isset($_POST['edit_mode'])) {
    echo "update";
    require_once '../mysqli_connect.php';
    print_r($_POST);
    echo $_POST['level'];
//}elseif (isset($_GET['edit_mode'])) {
////    echo "state changed";
////    var_dump($_GET);
//    edit_education($connection, $_GET['ed_id']);
////    exit();    
}

// find the type of education then load form per education type
function edit_education($connection, $ed_id) {
    require_once 'mysqli_connect.php';

    $edit_query = "select ed_level_id from user_education where user_education_id = " . $ed_id;

    $result = $connection->query($edit_query);
    $row = mysqli_fetch_assoc($result);
//    echo $row['ed_level_id'];
    echo '<form action="crud/edit_education.php" method="POST">';
       echo'<input type="hidden" name="ed_id" value="' . $ed_id. ' "/>';
    switch ($row['ed_level_id']) {
        case 0:
//            echo "<br>Edit HS";
            
            edit_hs($connection, $ed_id);

            break;
        case 1:
//            echo "<br>Edit some collage";
            edit_some_collage($connection, $ed_id);

            break;
        default:
//            echo "<br>Edit degree";
            edit_degree($connection, $ed_id);

            break;
    }
    echo'<button  type="submit" value="Submit" >Update</button>';
    echo '</form>';
}

function delete_education($connection, $ed_id) {
   
    $delete_query = "delete from user_education where user_education_id = " . $ed_id;
    if ($connection->query($delete_query) === TRUE) {
     $URL = "home.php?page=3";
        echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
        echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
} else {
    echo "Error deleting record: " . $connection->error;
    
}

$connection->close();

}

function edit_degree($connection, $ed_id) {
    $degree_query = "Select * from user_education
where user_education.user_education_id = $ed_id";

    $result = $connection->query($degree_query);
// loads degree infomation
    if (mysqli_num_rows($result) >= 1) {
        while ($row = mysqli_fetch_assoc($result)) {

            echo'<div class="form">';
            echo'<input type="hidden" name="edit_mode" value=1</>';
            echo '<label for="ed_level">Highest level of education completed?</label>';
            get_education_level($connection, $ed_id);
            echo '<br><label for="degree">What type of degree to you have?</label>';
            degree_edit($connection, $ed_id);
            echo '<br><label >What state was the school you attended?</label>';
            get_state($connection, $ed_id);
            echo '<br><label>What school did you attend?</label>';
            get_schools($connection, $ed_id);
            echo '<br><label>What year did you graduate?</label>';
            get_year($connection, $ed_id);
            echo '</div>';

        }
    }
}

function edit_some_collage($connection, $ed_id) {
// loads some collage
    $ed_query = "Select * from user_education
                        where user_education.user_education_id = $ed_id";

    $result2 = $connection->query($ed_query);
    if (mysqli_num_rows($result2) >= 1) {
        while ($row = mysqli_fetch_assoc($result2)) {

            echo'<div class="form">';
            echo'<input type="hidden" name="edit_mode" value=1 />';
            echo '<label for="ed_level">Highest level of education completed?</label>';
            get_education_level($connection, $ed_id);
            echo '<br><label >What state was the school you attended?</label>';
            get_state($connection, $ed_id);
            echo '<br><label>What school did you attend?</label>';
            get_schools($connection, $ed_id);
            echo '<br><label>What year did you graduate?</label>';
            get_year($connection, $ed_id);
            echo' </div>';

        }
    }
}

// show high school or GED information
function edit_hs($connection, $ed_id) {
    $hs_query = "Select * from user_education
                        where user_education.user_education_id = $ed_id";
    $result3 = $connection->query($hs_query);
    if (mysqli_num_rows($result3) == 1) {
        while ($row = mysqli_fetch_assoc($result3)) {

            echo'<div class="form"';
            echo'<input type="hidden" name="edit_mode" value=1</>';
                 echo '<label for="ed_level">Highest level of education completed?</label>';
            get_education_level($connection, $ed_id);
            echo '<br><label>What year did you graduate?</label>';
            get_year($connection, $ed_id);
            echo '</div>';

        }
    }
}

function degree_edit($connection, $ed_id) {
    $degree_query = "Select degree_name_id from user_education where user_education_id=" . $ed_id;
    $result = $connection->query($degree_query);
    $row = mysqli_fetch_assoc($result);
    $degree_query2 = "Select degree_name from user_degree_name where id = " . $row['degree_name_id'];
    $result2 = $connection->query($degree_query2);
    $row2 = mysqli_fetch_assoc($result2);
    $degree_type = substr($row2['degree_name'], 0, 3);
    $list_degree = "Select * from user_degree_name where degree_name like '" . $degree_type . "%'";
    $result3 = $connection->query($list_degree);

    echo "<select name=degree>";
    while ($row3 = mysqli_fetch_assoc($result3)) {
        if ($row3['id'] === $row['degree_name_id']) {
            echo"<option value=" . $row3['id'] . "selected>" . $row3['degree_name'] . "</option>";
        } else {
            echo"<option value=" . $row3['id'] . " >" . $row3['degree_name'] . "</option>";
        }
    }
    echo "</select>";
}

function get_education_level($connection, $ed_id) {
    $ed_level = "Select ed_level_id from user_education where user_education_id = $ed_id";
    $result = $connection->query($ed_level);
    $row = mysqli_fetch_assoc($result);
    $list_ed_level = "Select * from user_education_level";
    $result2 = $connection->query($list_ed_level);
    echo '<select name="level">';
    while ($row2 = mysqli_fetch_assoc($result2)) {
        if ($row2['ed_level_id'] === $row['ed_level_id']) {
            echo"<option value='" . $row2['ed_level_id'] . "' selected>" . $row2['ed_level'] . "</option>"; // need to row id
        } else {
            echo"<option value='" . $row2['ed_level_id'] . "'>" . $row2['ed_level'] . "</option>";
        }
    }
    echo "</select>";
}

function get_state($connection, $ed_id) {
    $state_query = "Select state from user_education where user_education_id = $ed_id";
    $result = $connection->query($state_query);
    $row = mysqli_fetch_assoc($result);
    $_SESSION['state'] = $row['state'];
    $list_state_query = "SELECT id, name, code FROM regions
                            where country_id = 230;";
//
    $result2 = $connection->query($list_state_query);
    echo '<select name="state" onchange="reload5_update(this.form)" >';
    echo'<option value="">Select State</option>';
    while ($row2 = mysqli_fetch_array($result2)) {
        if ($row2['code'] == $row['state']) {
            echo '<option value="' . $row2['code'] . '"selected >' . $row2['name'] . '</option>';
        } else {
            echo '<option value="' . $row2['code'] . '">' . $row2['name'] . '</option>';
        }
    }
    echo '</select>';
}

function get_schools($connection, $ed_id) {
    $school_query = "Select user_school_id from user_education where user_education_id = $ed_id";
    $result = $connection->query($school_query);
    $row = mysqli_fetch_assoc($result);
//    echo $row['user_school_id'];
    $get_schools_query = " select * from user_school where user_school_id =" . $row['user_school_id'];
    $result2 = $connection->query($get_schools_query);
    $row2 = mysqli_fetch_assoc($result2);

    $list_all_schools = " select * from user_school
                                where school_address like '%, " . $_SESSION['state'] . "%'";
    $result3 = $connection->query($list_all_schools);

    echo "<select name='school'>";
    while ($row3 = mysqli_fetch_assoc($result3)) {
        if ($row['user_school_id'] == $row3['user_school_id']) {
            echo"<option value=" . $row3['user_school_id'] . " selected>" . $row3['school_name'] . "</option>";
        } else {
            echo"<option value=" . $row3['user_school_id'] . ">" . $row3['school_name'] . "</option>";
        }
    }

    echo '</select>';
}

function get_year($connection, $ed_id) {
    $year_query = "select * from years";
    $result = $connection->query($year_query);
    $ed_year = "Select grad_yr from user_education where user_education_id = $ed_id";
    $result2 = $connection->query($ed_year);
    $row2 = mysqli_fetch_assoc($result2);
//echo $row2['grad_yr'];
    echo "<select name='year' >";
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['yr_id'] === $row2['grad_yr']) {
            echo '<option value=' . $row['yr_id'] . ' selected >' . $row['grad_year'] . '</option>';
        } else {
            echo '<option value=' . $row['yr_id'] . '>' . $row['grad_year'] . '</option>';
        }
    }
    echo'</select>';
}
