
<?php

if (isset($_POST['addImage'])) {
    //load upload page
    header("location:../home.php?page=11");
}

if (isset($_FILES['fileToUpload'])) {
    session_start();
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($_FILES["fileToUpload"]["name"], PATHINFO_EXTENSION));
    $target_dir = $_SESSION['image_path'];
    $ext = $imageFileType;
    // check file type 
    if (isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    } else {
        echo "<br> file size:  " . $_FILES["fileToUpload"]["size"];
        $uploadOk = 1;
    }
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    } else {
        echo "<br>file type: " . $imageFileType;
        $uploadOk = 1;
    }
    $newName = $_SESSION['lname'] . '_' . $_SESSION['fname'] . '_user_pic';
    $target_file = strtolower('../' . $target_dir . $newName . '.' . $ext);


    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    } else {
        If (save_image($_SESSION['user_id'], $target_file) == 1) {
            echo "<br>save to database";
           if(saveimagetoDB($newName, $target_dir, $ext)==1){
               header("location:../home.php?page=1");
        }
    }
}
}

function saveimagetoDB($newname, $target_dir, $ext) {
    require '../mysqli_connect.php';
    $newname .= "." . $ext;
    $newname = strtolower($newname);
    print_r($_SESSION);

    $saveImageData = "update user "
            . " set img_name ='" . $newname . "'"
            . " ,img_location ='" . $target_dir . "'"
            . " where user_id = " . $_SESSION['user_id'];
//    echo'<br>' . $saveImageData . '<br>';
    if ($connection->query($saveImageData) === TRUE) {
        echo "Record updated successfully";
        return 1;
    } else {
        echo "Error updating record: " . $connection->error;
        return 0;
    }

    $connection->close();
}

function getImage($connection, $userId) {
    require './mysqli_connect.php';
    $has_image = "select not IsNULL(img_name) as hasImage,img_name  
from user
where user_id = " . $_SESSION['user_id'];
    $result = $connection->query($has_image);
    $row = mysqli_fetch_assoc($result);
//    echo $row['hasImage'];
    if ($row['hasImage'] == 1 && $row['img_name'] !="") {
        $userPic = "select img_name, img_location 
                        from user
                        where user_id =" . $_SESSION['user_id'];
        $result = $connection->query($userPic);        
        $row = mysqli_fetch_assoc($result);
        $location = $row['img_location'] . $row['img_name'];
     //        echo $location;
        
           echo"<img src= '$location'  alt='userPic' id='image'/>";
    } else {

        echo'<img src="./image/whitepuzzle.jpg"  alt="no user pic found"/>';
    }
}

function create_user_path() {
    $user_folder = 'user/' . bin2hex($_SESSION['user_id'] . $_SESSION['fname'] . $_SESSION['lname']) . '/pic/';
    $user_video = 'user/' . bin2hex($_SESSION['user_id'] . $_SESSION['fname'] . $_SESSION['lname']) . '/video/';
    $vid_folder_ready = folder_exist($user_video);
    //dir for user video

    if ($vid_folder_ready !== true) {
        echo "folder not exist";
    } else {
//        echo "folder exist";
        $_SESSION['video_path'] = $user_video;
    }

    //dir for user pic
    $pic_folder_ready = folder_exist($user_folder);
    if ($pic_folder_ready !== true) {
        echo "folder not exist";
    } else {
//        echo "folder exist";
        $_SESSION['image_path'] = $user_folder;
    }
}

function folder_exist($folder) {
    //if directory exist return path if it does not make dir then return path
    // Get canonicalized absolute pathname
    $path = realpath($folder);

//     If it exist, check if it's a directory
    if ($path !== false AND is_dir($path)) {
        // Return canonicalized absolute pathname
        return true;
    } else {

        // Path/folder does not exist create it 
        if (!mkdir($folder, 0755, true)) {
//            return false;
            echo "crate failed";
        } else {
//            echo "Created " . $path;
            return true;
        }
    }
}

function save_image($user_id, $target_file) {
    echo '<br>' . $target_file;
    
//upload file location and save location to database
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        return 1;
    } else {

        print_r($_FILES);
        return 0;
    }
}
