<?php

//require('../mysqli_connect.php');
// get the information from the form and add it to the data base
// if it is not hs or some college reload so that additional degree infromation can be added
// when all degree information is entered then add video question.

function add_user_education($connection) {
    $testdate = date("Y-m-d H:i:s", time());
    $user_id = $_SESSION['user_id'];
    $ed_level = 0;
    $school_id = 9;
    $degree_type_id = 2;
    $from_date = $testdate;
    $to_date = $testdate;
    $grad_year = '2017';
    $degree_name_id = 3;
    $stmt = $connection->stmt_init();

    if (!($stmt = mysqli_prepare($connection, "INSERT INTO user_education(user_id, ed_level_id, user_school_id, degree_type_id, from_date,to_date,grad_yr,degree_name_id)"
            . " VALUES (?, ?, ?,?,?,?,?,?)"))) {
        echo 'prepare filed';
    }

    if (!($stmt->bind_param("iiiisssi", $user_id, $ed_level, $school_id, $degree_type_id, $from_date, $to_date, $grad_year, $degree_name_id))) {

        echo "Bind Failed";
    }
    if (!$stmt->execute()) {
        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
    }
}

function add_education($connection, $sql) {

    if ($connection->query($sql) === TRUE) {
//    echo "New record created successfully";
        ob_flush();
        $URL = "home.php?page=3";
        echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
        echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';

        exit();
    } else {
        echo "Error: " . $sql . "<br>" . $connection->error;
    }
    echo ' </div>';
    $connection->close();
}

function list_education($connection, $userid) {
    $ed_query = "select  user_education_id,user_education_level.ed_level ,years.grad_year, regions.name,user_school.school_name,
user_education.units_completed,user_education.in_school,user_education.area_of_study
from user_education
inner join user_school on user_school.user_school_id = user_education.user_school_id
inner join user_education_level on user_education_level.ed_level_id = user_education.ed_level_id
inner join years on years.yr_id = user_education.grad_yr
inner join regions on regions.code = user_education.state
where user_id = $userid && (user_education_level.ed_level_id = 1 || user_education_level.ed_level_id = 0) ;
 ";


    $degree_query = "select  user_education_id,user_school.school_name , user_education_level.ed_level , user_degree_name.degree_name, regions.name,years.grad_year
from user_education 
inner join user_school on user_school.user_school_id = user_education.user_school_id
inner join user_education_level on user_education_level.ed_level_id = user_education.ed_level_id
inner join user_degree_name on user_degree_name.id = user_education.degree_name_id
inner join regions on regions.code = user_education.state
inner join years on years.yr_id = user_education.grad_yr
where user_id = $userid
order by years.grad_year desc";

    $hs_query = "
select  user_education_id, user_education.area_of_study,years.grad_year
from user_education
inner join years on years.yr_id = user_education.grad_yr
where user_id = $userid && area_of_study like 'HS%'";

    $result = $connection->query($degree_query);
    $result2 = $connection->query($ed_query);
    $result3 = $connection->query($hs_query);
// loads degree infomation

    if (mysqli_num_rows($result) >= 1) {
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<form action="home.php?page=3" method="POST">';
            echo'<input type="hidden" name="ed_id" value=' . $row['user_education_id'] . ' </>';
            echo'<div id=ed-review>';
            echo'<div class="ed1"><label>' . $row['degree_name'] . '</label></div>';
            echo'<div class="ed2"><label>' . $row['grad_year'] . '</label></div>';
            echo'<div class="ed5"><button name="delete" class="btn clear-btn"><i class="fas fa-trash"></i></button></div>';
            echo'<div class="ed3"><label>' . $row['school_name'] . '</label></div>';
            echo'<div class="ed4"><label>' . $row['name'] . '</label></div>';
            echo'</div>';
            echo'</form>';
        }
    }
    // loads some collage
    if (mysqli_num_rows($result2) >= 1) {
     
        while ($row = mysqli_fetch_assoc($result2)) {
               echo '<form action="home.php?page=3" method="POST">';
            echo'<div id=ed-review>';
            echo'<input type="hidden" name="ed_id" value=' . $row['user_education_id'] . ' </>';
            if ($row['in_school'] == 0) {
//            echo'<div class="ed1"><label>' . $row['ed_level'] . '</label></div>';
                echo '<div class="ed1"><label>Last Attended ' . $row['school_name'] . '</label></div>';
            } else {
                echo '<div class="ed1"><label>Currently Attending: ' . $row['school_name'] . '</label></div>';
//                 echo'<div class="ed1"><label>' . $row['ed_level'] . '</label></div>';
            }
            echo'<div class="ed2"><label>' . $row['grad_year'] . '</label></div>';
           echo'<div class="ed5"><button name="delete"><i class="fas fa-trash"></i></button></div>';
            echo'<div class="ed3"><label>' . $row['area_of_study'] . '</label></div>';
            echo'<div class="ed4"><label>' . $row['units_completed'] . '</label></div>';
            echo'</div>';
            echo'</form>';
//            echo'<br>';
        }
    }
    // show high school or GED information

    if (mysqli_num_rows($result3) >= 1) {

        while ($row = mysqli_fetch_assoc($result3)) {
            echo '<form action="home.php?page=3" method="POST">';
            echo'<div id=ed-review>';
            echo'<input type="hidden" name="ed_id" value=' . $row['user_education_id'] . ' </>';
            echo'<div class="ed1"><label>HS/GED</label></div>';
            echo'<div class="ed2"><label>'.$row['grad_year'].'</label></div>';
           echo'<div class="ed5"><button name="delete"><i class="fas fa-trash"></i></button></div>';
           echo'</div>';
            echo'</form>';
        }
    }
}
