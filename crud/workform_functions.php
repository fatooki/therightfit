<!--<link href="css/fontawesome.min.css" rel="stylesheet" type="text/css"/>-->
<?php
//session_start();

/*
 * All Things related to the users work history
 * 12.29.18
 */

require('./mysqli_connect.php');
include('site_functions/form_functions.php');

// if work history exist load this form if not load blank page

function load_history($connection) {
    $userid = $_SESSION['user_id'];
    $work_query = "select countries.name, regions.name as state, cities.name as city, work_history.* 
                from work_history
                inner join countries on work_history.co_country = countries.id
                inner join regions on work_history.co_state = regions.id
                inner join cities on work_history.co_city = cities.id
                where user_id = $userid
                order by start_date DESC";


    $result = $connection->query($work_query);
    while ($row = mysqli_fetch_assoc($result)) {


        $start = new DateTime($row['start_date']);
        $end = new DateTime($row['end_date']);
         echo'<div class="user-info">';
        echo'<form action="crud/process_work.php" method="POST">';
        echo'  <div id="container">';
        
       
        echo "<div class='left-sm-text'><label>From: &nbsp; </label>" . $start->format('M-d-Y') . ""
        . "<input type='hidden' name='work_id' value=" . $row['work_id'] . "></div>";
        echo "<div class='right-sm-text'> <label>To:&nbsp;</label> " . $end->format('M-d-Y') . "</div>";

        echo '<div class="edit-text"><button name="edit" class="btn clear-button"><i class="fas  fa-pen"></i></button>'
        . '<button name="delete" class="btn clear-button"><i class="fa fa-trash"></i></button></div>';
        
        echo "<div class='left-text'><label>Company Name:&nbsp;</label> " . $row['co_name'] . "</div>";
         echo "<div class='left-text'><label>Job Title:&nbsp;</label> " . $row['job_title'] . "</div>";

//        get county code then
        echo "<div class='county-text'><label>County:</label><br>" . $row['name'] . "</div>";
        echo "<div class='state-text'><label>State:</label><br>" . $row['state'] . "</div>";
        echo "<div class='city-text'><label>City:&nbsp; </label><br>" . $row['city'] . "</div>";
        echo "<div class='desc-text'><p>" . $row['job_discription'] . "</p> </div>";
        echo '</div>';
        echo "</form>"
        . "</div>";
        echo '   <div class="form-bottom-area2">.</div>';
    }
}
?>


<?php

function edit_work_form($connection) {
// echo ' Edit funcntion  ' . $_SESSION['work_id'];
    $select_work = "select countries.name, regions.name as state, cities.name as city, work_history.* 
                from work_history
                inner join countries on work_history.co_country = countries.id
                inner join regions on work_history.co_state = regions.id
                inner join cities on work_history.co_city = cities.id
                where work_id =" . $_SESSION['work_id'];
    $result = $connection->query($select_work);
    $row = $result->fetch_assoc();
    $_SESSION['country_id'] = $row['co_country'];
    $_SESSION['state'] = $row['co_state'];
    $_SESSION['city'] = $row['co_city'];
    $_SESSION['how_many'] = $row['number_supervised'];   
    echo '<form method="POST"> 
        <input type=hidden name="edit" value=2>
           <div class="left-ed-text">
            <label for="start-date">Start Date</label>
            <input type="date" name="start_date" value= ' . $row['start_date'] . '>
        </div>
        <div class="right-ed-text">
            <label for="end_date">End Date</label>
            
            <input type="date" name="end_date"value= ' . $row['end_date'] . '>
        </div>
        <div class="left-text"><label for="co_name" >Company Name</label><br>
        <input type="text" name="co_name" value= "' . $row['co_name'] . '" ></div>';
    
    get_location($connection);
    
    echo'<div class="left-text"><label for="position_title" >Position Title </label><br><input type="text" name="position_title"
                                                                                                        value=" ' . $row['job_title'] . '" /></div>';
    if ($row["manager"]==1){
         echo' <div class="middle-text"><label for="manager" >Was this a Manager position?</label><br><input type="checkbox" name="manager"  value = 1 checked />';
    } else {
       echo' <div class="middle-text"><label for="manager" >Was this a Manager position?</label><br><input type="checkbox" name="manager" value = 0 >';
    }
//    function to get edit how many
     echo'</div>';

        get_how_many($connection);
        
        echo '<div class="text-area"><label for="job_discription" >Job Discription </label><br><textarea name="job_discription" >' . $row['job_discription'] . '</textarea></div>

        <button formaction="crud/updates.php">Update</button>
    </div>

</form>   ';
}

