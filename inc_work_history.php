
<SCRIPT language=JavaScript>
    function reload(form)
    {
        var val = form.ddCountry.options[form.ddCountry.options.selectedIndex].value;
        self.location = 'home.php?page=2&set=3&edit=2&countryID=' + val;
    }
    function reload3(form)
    {
        var val = form.ddCountry.options[form.ddCountry.options.selectedIndex].value;
        var val2 = form.ddRegion.options[form.ddRegion.options.selectedIndex].value;
        self.location = 'home.php?page=2&edit=2&set=3&countryID=' + val + '&regionID=' + val2;
    }

</script>
<?php
//session_start();
require('mysqli_connect.php');
include("crud/workform_functions.php");
?>


<?php
if (!isset($_GET['edit'])) {
    ?>

    <main>    
        <page>
            <page-left>
                <form action="crud/process_work.php" method="POST">
                       <div class="form-head-area">Work History</div>
                    <div id="form-container">
                     
                       <?php
                       include('location2.php');
                       ?>
                        <div class="left-text">
                            <label for="start-date">Start Date</label><br>
                            <input type="date" name="start_date" required>
                        </div>
                        <div class="right-text">
                            <label for="end_date">End Date</label><br>
                            <input type="date" name="end_date" required>
                        </div>
                        <div class="left-lg-text"><label for="co_name" >Company Name</label><br><input type="text" name="co_name" value="" required/></div>
                      
                    <!--</div>-->
                    <div class="left-lg-text">Position Title <label for="position_title" ></label><br><input type="text" name="position_title"
                                                                                                          value="" required/></div>
                    <div class="left-lg-text"><label for="manager" >Was this a Manager position?
                        <select name="manager" required>
                            <option value="" selected></option>
                            <option  value="0">No</option>
                            <option  value="1">Yes</option>
                        </select></label>
                    </div>
                    <div class="left-lg-text"><label for="how_many" >If yes how many did you manage?</label>
                        <select name="how_many" required>
                            <option value="1"> 1</option>
                            <option  value="2">2</option>
                            <option  value="3">3</option>
                            <option  value="4">4</option> 
                            <option  value="5">5</option>   
                            <option  value="6">6</option>   
                            <option  value="7">7</option> 
                            <option  value="8">8</option>
                            <option  value="9">9</option>
                            <option  value="10">10 +</option>
                        </select>
                    </div>
                    <div class="left-xlg-text"><label for="job_discription" >Job description  </label><br><textarea name="job_discription" required></textarea></div>
                    <input type="submit" class="button" value="Submit"/>
            
                   
                    </div>
                </form> 
                <div class="form-bottom-area">.</div>
            </page-left>
            <page-right> 
         
                <?php
                $found = true;
                if ($found) {
                    load_history($connection);
                } else {
                    echo"no history found";
                }
                ?>
                <!--</div>-->
             
            </page-right>
        </page>
    </main>
    <?php
} else {
    ?>
    <main>    
        
            <page-edit>
                <?php
                edit_work_form($connection);
                ?>
            </page-edit>
       
    </main>
    <?php
}
?>
