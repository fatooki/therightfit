<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.min.css" />
<link rek="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>-->
<!--<link href="css/question.css" rel="stylesheet" type="text/css"/>-->
<script src="https://kit.fontawesome.com/86c89ec40b.js"></script>
<!--<link href="css/button.css" rel="stylesheet" type="text/css"/>-->
<link href="css/progressbar.css" rel="stylesheet" type="text/css"/>
<script>

    $(function () {
        $(document).ready(function () {
            var bar = $('#bar')
            var percent = $('#percent');
            var status = $('#status');

            $('form').ajaxForm({
                beforeSend: function () {
                    status.empty();
                    var percentVal = '0%';
                    bar.width(percentVal);
                    percent.html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    percent.html(percentVal);
                    bar.width(percentVal);
                },
                complete: function (xhr) {
                    status.html(xhr.responseText);
                }
            });
        });
    });

   
</script>
<main>
    <div class="form-head-area">Select Question</div>
<?php
require 'mysqli_connect.php';


 
// get the number of answered question
    $num_of_rows = 10;
   
    If (isset($_GET['q_page']) && $_GET['q_page'] != 1) {
        $offset = $_GET['q_page'] * $num_of_rows;
        $limit = get_answered_questions($connection, 10);
        $start_pos = ($_GET['q_page'] * $num_of_rows) - $limit;
    } else {
        $offset = 0;
        $limit = get_answered_questions($connection, 10);
        $start_pos = ($offset * $num_of_rows);
    }

    get_questions($connection, $start_pos, $limit);

    echo '<div id="bararea">
            <div id="bar"></div>
            </div>
            <div id="percent"></div>
            <div id="status"></div>';
    
    echo '<div id="pagination">';
    get_pageNum();
    echo '</div>';
    ?>

</main>
<?php
function get_answered_questions($connection, $num_of_rows) {

    // if page number one or default (the max num per page -number of answered videos) will be the results size


    $answered_questions = "select  questions.question_id as q_id , catagory,question , video_response.question_id as vid_q_id,
                                video_response.user_id, response_id
                                from questions
                                left outer join video_response on  questions.question_id = video_response.question_id
                                where not isnull( video_response.question_id) and video_response.user_id =  59";
    $result2 = $connection->query($answered_questions);
    $answered = mysqli_num_rows($result2);
    $q_limit = $num_of_rows - $answered;
    while ($row2 = mysqli_fetch_assoc($result2)) {
        echo'<form  id="question-content"action="site_functions/process.php" method="post" enctype="multipart/form-data">';
        echo '<div class="question-col"><input type = "text" name = "response_id" value =" ' . $row2['response_id'] . ' " hidden="hidden" />' . $row2['question'] . '</div>';
        echo '<div class="file-col"><i class="fa fa-check center"></i></div>';
        echo '<div class="question-button-col"><input type="submit" value="redo" id="redo" /></div>';
        echo '</form>';
    }

    return $q_limit;
}

function get_questions($connection, $start, $limit) {
    $question_query = "select  questions.question_id as q_id , catagory,question , video_response.question_id as vid_q_id,video_response.user_id
                                                    from questions
                                                    left outer join video_response on  questions.question_id = video_response.question_id
                                                    where isnull( video_response.question_id)
                                                    limit $start, $limit";
  
    $result = $connection->query($question_query);
    $total_questions = mysqli_num_rows($result);
    while ($row = mysqli_fetch_assoc($result)) {
        echo'<form  id="question-content" action="site_functions/process.php" method="post" enctype="multipart/form-data">';
        echo '<div class="question-col"> <input type = "text" name = "q_id" value =" ' . $row['q_id'] . ' " hidden="hidden" /> ' . $row['question'] . '</div>';
        echo '<div class="file-col"><input type="file" name="files" id="files" /></div>';
        echo '<div class="question-button-col"><input type="submit" value="Upload File" id="upload"></div>';
        echo '</form>';
    }
}

function get_pageNum() {

    echo'<a href="">&laquo;</a>';
    $count = 5;
    for ($i = 1; $i <= $count; $i++) {
        if (isset($_GET['q_page']) && ($i == $_GET['q_page'])) {
            echo'<a href="home.php?page=4&q_page='. $i . '" class=active>' . $i . '</a>';
        } else {
            echo'<a href="home.php?page=4&q_page='. $i . '">' . $i . '</a>';
        }
    }

    echo'<a href="">&raquo;</a>';
}
?>
