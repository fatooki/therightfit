
<link href="css/progressbar.css" rel="stylesheet" type="text/css"/>
<?php
include('site_functions/question_functions.php');

require_once('mysqli_connect.php');
?>   
<link href="css/interview.css" rel="stylesheet" type="text/css"/>
<main>
    <?php
    get_question_table($connection);
    ?>              
</main>

<script>

    $(function () {
        $(document).ready(function () {
            var bar = $('#bar')
            var percent = $('#percent');
            var status = $('#status');

            $('form').ajaxForm({
                beforeSend: function () {
                    status.empty();
                    var percentVal = '0%';
                    bar.width(percentVal);
                    percent.html(percentVal);
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    percent.html(percentVal);
                    bar.width(percentVal);
                },
                complete: function (xhr) {
                    status.html(xhr.responseText);
                }
            });
        });
    });

   
</script>
