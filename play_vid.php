
<section>            
    <?php
    include 'site_functions/question_functions.php';
    require('mysqli_connect.php');
    loadPlayer();
    ?>

</section>

<aside>
    <div class="form-head-area"></div>
    <?php
    review_question_response($connection);
    ?>
    <div class="form-bottom-area"></div>
</aside>

<script>
    var vid = document.getElementById("videoarea");
    vid.autoplay = true;
    $(function () {
        $("#playlist li").on("click", function () {
            $("#videoarea").attr({
                "src": $(this).attr("movieurl"),
                "poster": "",
//                        "autoplay": "False"
            })
        })
        $("#videoarea").attr({
            "src": $("#playlist li").eq(0).attr("movieurl"),
            "poster": $("#playlist li").eq(0).attr("moviesposter"),
//                    "autoplay": "False"
        })
    })
</script>
