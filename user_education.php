<link href="css/education.css" rel="stylesheet" type="text/css"/>
<?php
//session_start();
require("./mysqli_connect.php");
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="site_js/site.js" type="text/javascript"></script>
<script src="site_js/site_update.js" type="text/javascript"></script>

<?php
include_once './crud/education_functions.php';
include'./crud/edit_education.php';
?>

<main>


    <?php
     $userid = $_SESSION['user_id'];
    if (isset($_POST['ed_id'])) {

        if (isset($_POST['edit'])) {
            edit_education($connection, $_POST['ed_id']);
            exit;
        }
        if (isset($_POST['delete'])) {
            delete_education($connection, $_POST['ed_id']);
            exit;
        }
    } else {
        ?>
        <ed-page> 
            <div class="ed-page-left">
                <div class="form-head-area">Education</div>
                <form action="" method="post"> 
                    <div id="ed-container">

                        <div class="text6">
                            <?php
                            require('mysqli_connect.php');

                            $select_ed = "SELECT ed_level_id, ed_level FROM user_education_level";
                            $result = $connection->query($select_ed);
                            ?>
                            <label for="ddEducation">What is your highest level of education?</label>
                            <select name="ddEducation"  onchange="reload(this.form)">
                                <option value="99">Select Education Level</option>
                                <?php
                                while ($row = mysqli_fetch_assoc($result)) {
                                    if ($row["ed_level_id"] == $_GET["ed_id"]) {
                                        echo '<option value="' . $row["ed_level_id"] . '"selected>' . $row["ed_level"] . '</option>';
                                    } else {
                                        echo '<option value="' . $row["ed_level_id"] . '">' . $row["ed_level"] . '</option>';
                                    }
                                }
                                ?>                
                            </select>
                        </div>

                        <?php
                        if (isset($_GET['ed_id']) or isset($_SESSION['ed_id'])) {
                            $ed_id = $_GET['ed_id'];
                            $_SESSION['$ed_id'] = $ed_id;
                           
                            switch ($ed_id) {
                                case 0: //HS- GED
                                    echo'<div class="text6">';
                                    $year_query = "SELECT yr_id, grad_year FROM years;";
                                    $result = $connection->query($year_query);
                                    ?>
                                    <label for="ddYear">In what year did receive your Diploma or GED?</label>
                                    <select name="ddYear" onchange="reload2(this.form)">
                                        <option value="">Select Year</option>
                <?php
                while ($row = mysqli_fetch_assoc($result)) {
                    if ($row['yr_id'] == $_GET['yr_id']) {
                        echo '<option value="' . $row["yr_id"] . '" selected>' . $row["grad_year"] . '</option>';
                    } else {
                        echo '<option value="' . $row["yr_id"] . '" >' . $row["grad_year"] . '</option>';
                    }
                }
                ?>
                                    </select>
                                </div>
                <?php
                if (isset($_GET['yr_id'])) {
                    echo '<div class="text6">';
                    echo "<h5>Video Response</h5>";
                    echo'<label>What are your education plans?</label>';
                    echo '<div class="text1"><input type="file" name="education_response" value="" /></div>';
                    echo'</div>';
                }
                break;
            case 1: // Some College
                echo '<div class="text6">';
                echo '<label>Are you currenlty enrolled in school?</label>';

                if (!isset($_GET['enrolled'])) {
                    echo'<select name = "enrolled" onchange="reload2a(this.form)">
                                                <option value= -1 selected>select</option>
                                                <option value = 1>Yes</option>
                                                <option value = 0>No</option>
                                                </select>';
                } else {
                    switch ($_GET['enrolled']) {
                        case 0:
                            echo'<select name = "enrolled" onchange="reload2a(this.form)">
                                                <option value= -1 >select</option>
                                                <option value = 1>Yes</option>
                                                <option value = 0 selected>No</option>
                                                </select>';
                          
                            echo '<label>What state did you last attend school in</label>';
                            list_state_some_collage($connection);

                            if (isset($_GET['state'])) {
                                echo '<label>What is the name of the school you attended</label>';
                                list_schools($connection, $_GET['state']);
                                echo '<label for="major">What was your major area of study?</label>';
                                echo'<input type = "text" name = "major"  />';
                                echo '<label>How many units did you complete?</label>';
                                echo'<input type = "number" name = "units_completed" min=1 max=70  />';
                                echo '<label>What year was your last attandance?</label>';
                                list_year2($connection);
                            }
                         
                            break; // No not in school
                        case 1;
                            echo'<select name = "enrolled" onchange="reload2a(this.form)">
                                                <option value= -1>select</option>
                                                <option value = 1 selected>Yes</option>
                                                <option value = 0>No</option>
                                                </select>';
                            echo '<label>What state are you attending school?</label>';
                            list_state_some_collage($connection);
                            if (isset($_GET['state'])) {
                                echo '<label>What school are you currently?</label>';
                                list_schools($connection, $_GET['state']);
                                echo '<label>What was your major area of study?</label>';
                                echo'<input type = "text" name = "major"  />';
                                echo '<label>When do you expect to graduate?</label>';
                                list_year2($connection);
                            }
                            break; //yes still in school
                        default;
                            echo'<select name = "enrolled" onchange="reload2a(this.form)">
                                                <option value= -1 selected>select</option>
                                                <option value = 1>Yes</option>
                                                <option value = 0>No</option>
                                                </select>';
                            break; // no option selected
                    }
                }
                     echo'</div>';
                break;
            case 2: // AS Degree
                echo'<div class="text6">';
                echo '<label for="degree">What type of degree to you have?</label>';
                degree($connection, $_GET['ed_id']);
                echo '<br><label >What state was the school you attended?</label>';
                list_state($connection);
                echo '<br><label>What school did you attend?</label>';
                if (isset($_GET['state'])) {
                    $state = $_GET['state'];
                } else {
                    $state = '';
                }
                list_schools($connection, $state);
                echo '</div>';
                list_year($connection);
                break;
            case 3: // BA/BS
                echo'<div class="text6">';
                echo '<label for="degree">What type of degree to you have?</label>';
                degree($connection, $_GET['ed_id']);
                echo '<br><label >What state was the school you attended?</label>';
                list_state($connection);
                echo '<br><label>What school did you attend?</label>';
                if (isset($_GET['state'])) {
                    $state = $_GET['state'];
                } else {
                    $state = '';
                }
                list_schools($connection, $state);
                echo '</div>';
                list_year($connection);


                break;
            case 4: //Master
                echo'<div class="text6">';
                echo '<label for="degree">What type of degree to you have?</label>';
                degree($connection, $_GET['ed_id']);
                echo '<br><label >What state was the school you attended?</label>';
                list_state($connection);
                echo '<br><label>What school did you attend?</label>';
                if (isset($_GET['state'])) {
                    $state = $_GET['state'];
                } else {
                    $state = '';
                }
                list_schools($connection, $state);
                echo '</div>';
                list_year($connection);

                break;
            case 5: //Dr
                echo'<div class="text6">';
                echo '<label for="degree">What type of degree to you have?</label>';
                degree($connection, $_GET['ed_id']);
                echo '<br><label >What state was the school you attended?</label>';
                list_state($connection);
                echo '<br><label>What school did you attend?</label>';
                if (isset($_GET['state'])) {
                    $state = $_GET['state'];
                } else {
                    $state = '';
                }
                list_schools($connection, $state);
                echo '</div>';

                list_year($connection);
                break;
            default:
        }
    } else {
//                                echo'<div class="text6"></div>';
    }
    ?>
                    <button-area>
                        <input type="submit" class="button" value="Submit">
                    </button-area>


    <?php
//
//    this is where the insert statements for each of the education types are created
//    
    if (isset($_POST['ddEducation'])) {

        switch ($_POST['ddEducation']) {
            case 0:

                $ed_id = $_POST['ddEducation'];
                $degree = "HS/GED";
                $grad_year = $_POST['ddYear'];
                $insert_education = "INSERT INTO user_education "
                        . "(user_id, ed_level_id,degree_type_id, grad_yr,area_of_study ) "
                        . "VALUES($userid,$ed_id, $ed_id,$grad_year, '$degree');";
                echo $insert_education;
                add_education($connection, $insert_education);
                break; // no selections
            case 1:
                // split yes and no
                // 1 = yes 0 = no
                if (isset($_GET['enrolled']) && $_GET['enrolled'] == 0) {
//                                        echo "Not In school";
                    $ed_id = $_POST['ddEducation'];
                    $enrolled = $_POST['enrolled']; // in school yes or no 
                    $school = $_POST['schools'];
                    $major = $_POST['major']; //if school is yes the get major else get units that have been completed and major while in school
                    $grad_year = $_POST['ddYear'];
                    $units = $_POST['units_completed'];
                    $state = $_POST['state'];
                    $insert_education = "INSERT INTO user_education "
                            . "(user_id, ed_level_id,in_school, grad_yr, area_of_study,units_completed,state,user_school_id) "
                            . "VALUES($userid,$ed_id, $enrolled,$grad_year, '$major',$units,'$state',$school);";
                } else {
//                                        echo "In school";
                    $ed_id = $_POST['ddEducation'];
                    $enrolled = $_POST['enrolled']; // in school yes or no 
                    $school = $_POST['schools'];
                    $major = $_POST['major']; //if school is yes the get major else get units that have been completed and major while in school
                    $grad_year = $_POST['ddYear'];
                    $state = $_POST['state'];
                    $insert_education = "INSERT INTO user_education "
                            . "(user_id, ed_level_id,in_school, grad_yr, area_of_study,state,user_school_id) "
                            . "VALUES($userid,$ed_id, $enrolled,$grad_year, '$major','$state',$school);";
                }
//                                    print_r($_POST);
//                                    echo $insert_education;
                add_education($connection, $insert_education);
                break;  //some college
            case 2:
                $ed_id = $_POST['ddEducation'];
                $degree = $_POST['degree'];
                $school = $_POST['schools'];
                $state = $_POST['state'];
                $grad_year = $_POST['ddYear'];

                $insert_education = "INSERT INTO user_education "
                        . "(user_id, ed_level_id, user_school_id, degree_type_id, grad_yr, degree_name_id,state) "
                        . "VALUES($userid,$ed_id, $school, $degree, $grad_year, $degree,'$state');";
                add_education($connection, $insert_education);
//print_r($_POST);
                break; // AS
            case 3:
                $ed_id = $_POST['ddEducation'];
                $degree = $_POST['degree'];
                $school = $_POST['schools'];
                $state = $_POST['state'];
                $grad_year = $_POST['ddYear'];

                $insert_education = "INSERT INTO user_education "
                        . "(user_id, ed_level_id, user_school_id, degree_type_id, grad_yr, degree_name_id,state) "
                        . "VALUES($userid,$ed_id, $school, $degree, $grad_year, $degree,'$state');";
                add_education($connection, $insert_education);
//print_r($_POST);
                break;  //BS           

                $insert_education = "INSERT INTO user_education "
                        . "(user_id, ed_level_id, user_school_id, degree_type_id, grad_yr, degree_name_id,state) "
                        . "VALUES($userid,$ed_id, $school, $degree, $grad_year, $degree,'$state');";
//                                    add_education($connection, $insert_education);
                break; //masyer
            case 4:
                $ed_id = $_POST['ddEducation'];
                $degree = $_POST['degree'];
                $school = $_POST['schools'];
                $state = $_POST['state'];
                $grad_year = $_POST['ddYear'];

                $insert_education = "INSERT INTO user_education "
                        . "(user_id, ed_level_id, user_school_id, degree_type_id, grad_yr, degree_name_id,state) "
                        . "VALUES($userid,$ed_id, $school, $degree, $grad_year, $degree,'$state');";
                add_education($connection, $insert_education);
//print_r($_POST);
                break; //masyer
            case 5:
                $ed_id = $_POST['ddEducation'];
                $degree = $_POST['degree'];
                $school = $_POST['schools'];
                $state = $_POST['state'];
                $grad_year = $_POST['ddYear'];

                $insert_education = "INSERT INTO user_education "
                        . "(user_id, ed_level_id, user_school_id, degree_type_id, grad_yr, degree_name_id,state) "
                        . "VALUES($userid,$ed_id, $school, $degree, $grad_year, $degree,'$state');";
                add_education($connection, $insert_education);
//       
                break;    //dr
            default:
                echo "default";
//                $ed_id = $_POST['ddEducation'];
//                $degree = "HS/GED";
//                $grad_year = $_POST['ddYear'];
//                $insert_education = "INSERT INTO user_education "
//                        . "(user_id, ed_level_id,degree_type_id, grad_yr,degree_name_id ) "
//                        . "VALUES($userid,$ed_id, $ed_id,$grad_year, $degree);";
//                //add_education($connection, $insert_education);
                break; // no selections
        }
    }
    ?>




            </div>   


            <div class="form-bottom-area2">.</div>

            </form>

            </div>

            <div class='page-right'>
                <div class="form-head-area">Education List</div>
    <?php
        list_education($connection, $userid);
    ?>

            </div>     




        </ed-page> 

    <?php
}
?>
</main>


<?PHP

function degree($connection, $type) {
//    echo "degree " . $type ;
    switch ($type) {
        case 2:
            $degree_type = "Assoc";
            break;
        case 3:
            $degree_type = "Bachelor";
            break;
        case 4:
            $degree_type = "Master";
            break;
        case 5:
            $degree_type = "Doc";
            break;
        default:
    }
    $degree_query = "SELECT * "
            . "FROM user_degree_name"
            . " where degree_name like'" . $degree_type . "%'";
    $result = $connection->query($degree_query);
    echo'<select name = "degree">';
    echo'<option value="">Select Degree</option>';
    while ($row = mysqli_fetch_assoc($result)) {
        if (isset($_GET['degree']) && ($row['id'] == $_GET['degree'])) {
            echo '<option value=' . $row["id"] . ' selected >' . $row['degree_name'] . '</option>';
        } else {
            echo '<option value=' . $row["id"] . '>' . $row['degree_name'] . '</option>';
        }
    }
    echo '</select>';
}

function list_schools($connection, $state) {
    $_SESSION['state'] = $state;
    $school_query = "SELECT user_school_id, school_name, school_address FROM user_school
                            where school_address like '%, " . $state . "%';";
    $result = $connection->query($school_query);
    echo '<select name="schools" >';
    echo'<option value="">Select School</option>';
    while ($row = mysqli_fetch_array($result)) {

        echo '<option value=' . $row['user_school_id'] . '>' . trim($row['school_name']) . '</option>';
    }
    echo '</select>';
}

function list_state($connection) {
    $state_query = "SELECT id, name, code FROM regions
                            where country_id = 230;";

    $result = $connection->query($state_query);
    if (isset($_GET['state'])) {
        $state = $_GET['state'];
    } else {
        $state = "";
    }
    echo '<select name="state" onchange="reload5(this.form)">';
    echo'<option value="">Select State</option>';
    while ($row = mysqli_fetch_array($result)) {
        if ($row['code'] == $state) {
            echo '<option value="' . $row['code'] . '"selected >' . $row['name'] . '</option>';
        } else {
            echo '<option value="' . $row['code'] . '">' . $row['name'] . '</option>';
        }
    }
    echo '</select>';
}

function list_state_some_collage($connection) {
    $state_query = "SELECT id, name, code FROM regions
                            where country_id = 230;";

    $result = $connection->query($state_query);
    if (isset($_GET['state'])) {
        $state = $_GET['state'];
    } else {
        $state = "";
    }
    $_SESSION['state'] = $state;
    echo '<select name="state" onchange="reload6(this.form)">';
    echo'<option value="">Select State</option>';
    while ($row = mysqli_fetch_array($result)) {
        if ($row['code'] == $state) {
            echo '<option value="' . $row['code'] . '"selected >' . $row['name'] . '</option>';
        } else {
            echo '<option value="' . $row['code'] . '">' . $row['name'] . '</option>';
        }
    }
    echo '</select>';
}

function list_year($connection) {
    $year_query = "SELECT yr_id, grad_year FROM years;";
    $result = $connection->query($year_query);
    ?>
    <div class="text6">
        <label for="ddYear">What year did you graduate?</label>
        <select name="ddYear" onchange="">
            <option value="">Select Year</option>
    <?php
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['yr_id'] == $_GET['yr_id']) {
            echo '<option value="' . $row["yr_id"] . '" selected>' . $row["grad_year"] . '</option>';
        } else {
            echo '<option value="' . $row["yr_id"] . '" >' . $row["grad_year"] . '</option>';
        }
    }
    ?>
        </select>
    </div>


    <?php
}

function list_year2($connection) {
    $year_query = "SELECT yr_id, grad_year FROM years;";
    $result = $connection->query($year_query);
    ?>
    <div class="text6">
        <!--        <label for="ddYear">What year did you graduate?</label>-->
        <select name="ddYear">
            <option value="">Select Year</option>
    <?php
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['yr_id'] == $_GET['yr_id']) {
            echo '<option value="' . $row["yr_id"] . '" selected>' . $row["grad_year"] . '</option>';
        } else {
            echo '<option value="' . $row["yr_id"] . '" >' . $row["grad_year"] . '</option>';
        }
    }
    ?>
        </select>
    </div>


    <?php
}
?>




