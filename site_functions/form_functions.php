<?php

///Common Functions for all forms
//Ray Graves
//12.19.18
require('mysqli_connect.php');

function get_country($connection) {
    if (isset($_GET['countryID'])) {
        $c_ID = $_GET['countryID'];
    } else {
        if (isset($_SESSION['country_id'])) {
            $c_ID = $_SESSION['country_id'];
        }
    }
//    echo $c_ID;

    $countyQuery = "SELECT id, name, code
                    FROM countries;";
    $result = $connection->query($countyQuery);
    echo '<div class="left-text" >'
  .'  <label for="ddCountry">Select County</label>';
                    
                      
    echo'<select name ="ddCountry" onchange="reload(this.form)">'
    . '<option value="">Select</option>';

    while ($row = mysqli_fetch_assoc($result)) {
        If ($row['id'] == $c_ID) {
            echo "<option value=" . $row['id'] . " selected >" . $row['name'] . "</option>";
        } else {
            echo "<option value=" . $row['id'] . " >" . $row['name'] . "</option>";
        }
    }
    echo '</select>';
    echo"
   
    </div>";
}

function get_region($connection, $countryid) {
    if (isset($_GET['regionID'])) {
        $r_ID = $_GET['regionID'];
    } else {
        $r_ID = 0; //$_SESSION['state'];
    }
    $regionQuery = "SELECT id, name, code, country_id
                    FROM regions
                    where country_id = $countryid;";

    echo '<div class="left-text">
                    <label for="ddRegion" class="cols-sm-2 control-label">Select State/Region</label>
                    <div class="cols-sm-10">
                        <div class="input-group">';
    echo'<select name ="ddRegion" onchange="reload3(this.form)">
                <option value="">Select State</option>';

    $result = $connection->query($regionQuery);
    while ($row = mysqli_fetch_assoc($result)) {
        If ($row['id'] == $r_ID) {
            echo "<option value=" . $row['id'] . " selected >" . $row['name'] . "</option>";
        } else {
            echo "<option value=" . $row['id'] . " >" . $row['name'] . "</option>";
        }
    }

    echo '</select>';

    echo"</div>
    </div>
    
    </div>";
}

function get_city($connection, $countryid, $regionid) {
    if (isset($_SESSION['city'])) {
        $city_id = $_SESSION['city'];
    } else {
        $city_id = "";
    }
//echo $city_id;
    $stateQuery = "SELECT id, region_id, country_id, latitude, longitude, name
                    FROM cities
                    where region_id = $regionid && country_id=$countryid "
            . "order by id asc";
//    echo $stateQuery;
    echo '<div class="left-text">
                    <label for="ddCity" class="cols-sm-2 control-label">Select City</label>
                    <div class="cols-sm-10">
                        <div class="input-group">';
    echo '<select name = "ddCity" >
                <option value="">Select City</option>';





    $result = $connection->query($stateQuery);
    while ($row = mysqli_fetch_assoc($result)) {

        if ($row['id'] == $city_id) {
            echo "<option value=" . $row['id'] . " selected >" . $row['name'] . "</option>";
        } else {

            echo "<option value=" . $row['id'] . " >" . $row['name'] . "</option>";
        }
    }



    echo '</select>';

    echo"</div>
    </div>
    </div>
    ";
}

function get_work_country($connection) {
    if (isset($_GET['countryID'])) {
        $c_ID = $_GET['countryID'];
    } else {
        $c_ID = 0;
    }
    $countyQuery = "SELECT id, name, code
                    FROM countries;";
    $result = $connection->query($countyQuery);
    echo '<div class="left-text">
                    <label for="ddCountry" class="control-label">Select County</label>                  
                        <div class="input-group">';
    echo'<select name ="ddCountry" onchange="reload(this.form)">'
    . '<option value="">Select</option>';
    while ($row = mysqli_fetch_assoc($result)) {
        If ($row['id'] == $c_ID) {
            echo "<option value=" . $row['id'] . " selected >" . $row['name'] . "</option>";
        } else {
            echo "<option value=" . $row['id'] . " >" . $row['name'] . "</option>";
        }
    }
    echo '</select>';
    echo"</div></div>";
}

function get_work_region($connection, $countryid) {
    if (isset($_GET['regionID'])) {
        $r_ID = $_GET['regionID'];
    } else {
        $r_ID = 0;
    }
    //get_country($connection);
    $regionQuery = "SELECT id, name, code, country_id
                    FROM regions
                    where country_id = $countryid;";
    //echo $regionQuery ;
    echo '<div class="left-text">
                    <label for="ddRegion" class=" control-label">Select State/Region</label>
                   
                        <div class="input-group">';
    echo'<select name ="ddRegion" onchange="reload3(this.form)">
                <option value="">Select State</option>';

    $result = $connection->query($regionQuery);
    while ($row = mysqli_fetch_assoc($result)) {
        If ($row['id'] == $r_ID) {
            echo "<option value=" . $row['id'] . " selected >" . $row['name'] . "</option>";
        } else {
            echo "<option value=" . $row['id'] . " >" . $row['name'] . "</option>";
        }
    }

    echo '</select>';

    echo"</div>
     </div>";
}

function get_work_city($connection, $countryid, $regionid) {
    $stateQuery = "SELECT id, region_id, country_id, latitude, longitude, name
                    FROM cities
                    where region_id = $regionid && country_id=$countryid ";
    // echo $stateQuery;
    echo '<div class="left-text">
                    <label for="ddCity" class="control-label">Select City</label>
                      <div class="input-group">';
    echo'<select name = "ddCity" >
                <option value="">Select City</option>
                <option value="1">city</option>
                 <option value="1">city</option>';
    $result = $connection->query($stateQuery);
    while ($row = mysqli_fetch_assoc($result)) {

        echo "<option value=" . $row['id'] . " >" . $row['name'] . "</option>";
    }

    echo '</select>';

    echo"</div>
     </div>";
}

function get_how_many($connection) {
//    
//    $sql = "Select number_supervised  from work_history where work_id = " . $_SESSION['work_id'];
//    $result = $connection ->qurey($sql);
    echo'<div class ="right-text"><label for="how_many" >If yes how many did you manage?</label>
            <select name="how_many">
                <option value="1">1</option>
                <option  value="2">2</option>
                <option  value="3">3</option>
                <option  value="4">4</option> 
                 <option  value="5">5</option>
                  <option  value="6">6</option>
                   <option  value="7">7</option> 
                   <option  value="8">8</option>
                    <option  value="9">9</option>
                     <option  value="10">10+</option>
                   
            </select>
        </div> ';
}

function get_location($connection) {
    //get county , state and city from work history 
    // create drop down boxes for each that should trigger the javasript for changes
    $select_work = "select countries.name, regions.name as state, cities.name as city, work_history.* 
                from work_history
                inner join countries on work_history.co_country = countries.id
                inner join regions on work_history.co_state = regions.id
                inner join cities on work_history.co_city = cities.id
                where work_id =" . $_SESSION['work_id'];
    $result = $connection->query($select_work);
    $row = $result->fetch_assoc();
    $_SESSION['country_id'] = $row['co_country'];
    $_SESSION['state'] = $row['co_state'];
    $_SESSION['city'] = $row['co_city'];
    $_SESSION['how_many'] = $row['number_supervised'];
    //country select box need to call a function for each of the drop downs
    if (!isset($_GET['countryID'])) {
        dd_country($connection);
    } else {
        $_SESSION['country_id'] = $_GET['countryID'];
        dd_country($connection);
    }

    dd_state($connection);
    dd_city($connection);
}

function dd_country($connection) {
    $country_query = "Select * from countries";
    $result = $connection->query($country_query);
    echo '<div class="left-text">
                    <label for="ddCountry" class="control-label">Select Country</label>
                      <div class="input-group">';
    echo'<select name = "ddCountry" onchange="reload(this.form)">'
    . '<option value="">Select</option>';

    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['id'] == $_SESSION['country_id']) {
            echo "<option value =" . $row['id'] . " selected >" . $row['name'] . "</option>";
        } else {
            echo "<option value =" . $row['id'] . ">" . $row['name'] . "</option>";
        }
    }
    echo "</select>";
}

function dd_state($connection) {

    if (!isset($_GET['countryID'])) {
        $state_query = "Select * from regions";
    } else {

        $state_query = "Select * from regions where country_id = " . $_GET['countryID'];
    }
    if (isset($_GET['regionID'])) {
        $_SESSION['state'] = $_GET['regionID'];
    }
    $result = $connection->query($state_query);
    echo '<div class="left-text">
                    <label for="ddRegion" class="control-label">Select State/Region</label>
                      <div class="input-group">';
    echo'<select name = "ddRegion" onchange="reload3(this.form)">'
    . '<option value="">Select</option>';

    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['id'] == $_SESSION['state']) {
            echo "<option value =" . $row['id'] . " selected >" . $row['name'] . "</option>";
        } else {
            echo "<option value =" . $row['id'] . ">" . $row['name'] . "</option>";
        }
    }
    echo "</select>";
}

function dd_city($connection) {
    if (isset($_GET['regionID'])) {
        $_SESSION['state'] = $_GET['regionID'];
        $city_query = "Select * from cities where country_id = " . $_SESSION['country_id'] . "&& region_id = " . $_GET['regionID'];
    } else {
        $city_query = "Select * from cities where country_id = " . $_SESSION['country_id'] . "&& region_id = " . $_SESSION['state'];
    }

    $result = $connection->query($city_query);
    echo '<div class="left-text">
                    <label for="ddCity" class="control-label">Select City</label>
                      <div class="input-group">';
    echo'<select name = "ddCity" >'
    . '<option value="">Select</option>';
    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['id'] == $_SESSION['city']) {
            echo "<option value =" . $row['id'] . " selected >" . $row['name'] . "</option>";
        } else {
            echo "<option value =" . $row['id'] . ">" . $row['name'] . "</option>";
        }
    }
    echo "</select>";
}

?>
