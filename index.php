<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link href="css/landing.css" rel="stylesheet" type="text/css"/>  
        <link href="css/button.css" rel="stylesheet" type="text/css"/>
        <script src="old_Scripts/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js" type="text/javascript"></script>

        <title>The Right Fit</title>
    </head>

    <body>
        <nav>
            <div class="menu">
                <img src="image/puzzle.png" alt="" height="50" width="50"/>
                <div class="label">The Right Fit</div>
                <div class="spacer"></div>
                <div class="item"><span>Contact</span></div>
                <div class="item"><span>Registration</span></div>
                <div class="item" onclick="window.location.href = 'home.php';"><span>Login</span></div>
            </div>
        </nav>    
        <main>
            <section class="cd-section cd-section--bg-fixed">
                <h1>Welcome!</h1>
                <div class="text-area">
                    <p>Like putting together, a jigsaw puzzle, employers spend hours, days even months looking for that right candidates to begin their interview process. 
                        Are you that missing piece? With this site you will be able to explain to employer why you are The Right Fit. 
                        The Right Fit gives job seekers, a platform to introduce themselves to potential employers 
                        give a short video response for your work-history, educational accomplishments and goals, and also, answers to some standard interview questions.</p>
                </div>
                <div id="button-grid">
                    <button type="button" class="btn-left" onclick="window.location.href = 'home.php?page=7';">Register</button>
                    <button class="btn-right" onclick="window.location.href = 'home.php';">Login</button>
                </div>
            </section>
            <section class="cd-section-border"> </section>
            <section class="cd-section cd-section--bg-fixed">
                <div id="section-grid-2">
                    <div class="col-main text-area-2">                     

                        <p>Employer can receive hundreds of resume for each position they announced. 
                            Don’t just send a word document or a PDF file, make yours stand out, 
                            send them a link to your online video resume and interview.</p>                   

                    </div>


                </div>
                <div id="button-grid">
                    <button type="button" class="btn-left" onclick="window.location.href = 'home.php?page=7';">Register</button>
                    <button class="btn-right" onclick="window.location.href = 'home.php';">Login</button>
                </div>


            </section>
            <section class="cd-section-border"></section>          
            <section id="section-grid" >
                <div class="column1">
                    <div class="text-area-dark">
                        <h1>The Right Fit</h1>
                        <p>Making a good first impression on a traditional interview is one 
                            of the most stressful events in a person’s life. 
                            The Right Fit gives you a platform to show your best self.

                        </p> 

                        <ul>
                            <li>Complete the registration</li>
                            <li>create a video response</li>
                            <li>upload video</li>
                            <li>send link to employers</li>                            
                        </ul>

                    </div>
                </div>
                <div class="column3">
                    <img src="image/puzzle-back2.jpg" alt="" class="img2"/>
                </div>
                <div id="button-grid">
                    <div class="column5">
                        <button type="button" class="btn-left" onclick="window.location.href = 'home.php?page=7';">Register</button>
                        <button class="btn-right" onclick="window.location.href = 'home.php';">Login</button>
                    </div>
                </div>
            </section>
            <section class="cd-section-border"></section>
            <section class="cd-section cd-section--bg-fixed">
                <h1>Welcome Page 4</h1>
                <div class="text-area">
                    <p></p>
                </div>
                <div id="button-grid">
                    <button type="button" class="btn-left" onclick="window.location.href = 'home.php?page=7';">Register</button>
                    <button class="btn-right" onclick="window.location.href = 'home.php';">Login</button>
                </div>
            </section>           
            <section class="cd-section-border"></section>
        </main>
        <footer>footer</footer>
    </body>
</html>
